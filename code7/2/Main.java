import java.util.ArrayList;
import java.util.HashMap;
import java.util.Scanner;

public class Main{
    public static ArrayList<Price> createListProduct(){
        Price p1 = new Price("Pulpen","Warna Hitam",3000,"IDR");
        Price p2 = new Price("Pensil","2B",2000,"IDR");
        Price p3 = new Price("Penghapus","Warna Putih",2500,"IDR");
        Price p4 = new Price("HVS","Isi 20",1000,"IDR");
        Price p5 = new Price("Lem","100 Gr",4000,"IDR");
        Price p6 = new Price("Spidol","Permanen",5000,"IDR");

        
        ArrayList<Price> listProduct = new ArrayList<Price>();
        listProduct.add(p1);
        listProduct.add(p2);
        listProduct.add(p3);
        listProduct.add(p4);
        listProduct.add(p5);
        listProduct.add(p6);
        return listProduct;
    }

    public static void printListProduct(ArrayList<Price> listProduct){
        for(int i =0; i <listProduct.size(); i ++){
            System.out.println(String.valueOf(i+1) + ". " + listProduct.get(i).getName());
            System.out.println("Deksripsi : " + listProduct.get(i).getDesc());
            System.out.println("Harga : " + String.valueOf(listProduct.get(i).getAmount()) + "\n");
        }
    }


    public static String bonusProduct(HashMap<Integer, Integer> keranjang){
        HashMap<Integer, Integer> hargaProduk = new HashMap<Integer, Integer>();
        ArrayList<Price> listProduct = createListProduct();

        for(int i: keranjang.keySet()){
            hargaProduk.put(i,listProduct.get(i).getAmount());
        }

        int produkTermurah = hargaProduk.get(1);
        int produkTermurahId = 0;
        for(int i = 2 ; i <= hargaProduk.size() ; i++){
            produkTermurah = Math.min(produkTermurah, hargaProduk.get(i));
        }
        for(int i:hargaProduk.keySet()){
            if(hargaProduk.get(i) == produkTermurah ){
                produkTermurahId = i;
            }

        }
        
        return listProduct.get(produkTermurahId).getName();

    }

    public static double totalHarga(HashMap<Integer, Integer> keranjang){
        ArrayList<Price> listProduct = createListProduct();
        double harga =0;
        double totalHarga =0;
        
        for(int i:keranjang.keySet()){
            harga = listProduct.get(i).getAmount() * keranjang.get(i);
            if(keranjang.get(i) == 6){
                harga = listProduct.get(i).getAmount() * keranjang.get(i) * 0.8;
            }
            totalHarga = totalHarga + harga;
        }

        return totalHarga;
    }

    public static void printInvoice(HashMap<Integer, Integer> keranjang){
        System.out.println(" ");
        
        // Bonus Barang
        if(keranjang.size() >= 5){
            System.out.println("Bonus Barang : " + bonusProduct(keranjang));
        }
        System.out.println("Total Harga : " + totalHarga(keranjang));


    }
    public static void main(String[] args) {

        ArrayList<Price> listProduct = createListProduct();
        HashMap<Integer, Integer> keranjang = new HashMap<Integer, Integer>();

        Scanner userInput = new Scanner(System.in);

        System.out.println(listProduct.get(0).getName());
        
        System.out.println("Produk yang tersedia : ");
        for(;;){
            printListProduct(listProduct);
            System.out.println("KERANJANG :");

            for(int i: keranjang.keySet()){
                if(keranjang.size() == 0){
                    break;
                }
                System.out.println(listProduct.get(i).getName() + (" (QTY : " + keranjang.get(i) + ")" ));
            }
            System.out.println();

            System.out.println("Silahkan tekan 0 Jika Sudah Selesai Belanja");
            System.out.println("Pilih Produk Yang Ingin Di Beli :");
            int pilihProduk = userInput.nextInt() ;
            if(pilihProduk == 0){
              
                break;
            }

            System.out.println("Jumlah Produk yang diinginkan");
            int qtyProduk = userInput.nextInt();

            keranjang.put(pilihProduk - 1, qtyProduk);
        }
        if(keranjang.size() > 0){
            printInvoice(keranjang);
        }
       
       
    }
}