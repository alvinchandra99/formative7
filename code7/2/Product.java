public class Product {
    private int id =0;
    private String name;
    private String description;

    Product(String name, String description){
        this.id = id++;
        this.name = name;
        this.description = description;
    }

    public String getName(){
        return name;
    }

    public int getId(){
        return id;
    }

    public String getDesc(){
        return description;
    }

    
}
