

public class Price extends Product {
    private int id =0;
    private int amount;
    private String valuta;
    private int productId;

    public Price(String name, String description, int amount, String valuta){
        super(name,description);
        this.id = id++;
        this.amount = amount;
        this.valuta = valuta;
        this.productId = super.getId();

    }
    public int getPriceId(){
        return id;
    }
    public int getAmount(){
        return amount;
    }
    public String getValuta(){
        return valuta;
    }

    
    
}
