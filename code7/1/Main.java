public class Main{
    public static void main(String[] args) {
        Karyawan1 k1 = new Karyawan1();
        Karyawan2 k2 = new Karyawan2();
        Karyawan3 k3 = new Karyawan3();

        k1.start();
        k2.start();
        k3.start();

        try { 
            k1.join();
            k2.join();
            k3.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}