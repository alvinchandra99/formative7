import java.util.LinkedHashMap;
import java.io.FileWriter;
import java.io.IOException;

public class Karyawan1 extends Thread {
    
    int getMonth(String tgl){
        if(tgl.substring(3,4) == "0"){
            return Integer.parseInt(tgl.substring(4, 5)) ;
        }
        return Integer.parseInt(tgl.substring(3, 5));
    }

    String getMonthName(int month){
        switch(month){
            case 1:
                return "Januari";
            case 2:
                return "Febuari";
            case 3:
                return "Maret";
            case 4:
                return "April";
            case 5:
                return "Mei";
            case 6:
                return "Juni";
            case 7:
                return "Juli";
            case 8:
                return "Agustus";
            case 9:
                return "September";
            case 10:
                return "Oktober";
            case 11:
                return "November";
            case 12:
                return "Desember";
        }
        return "";
    }
 
     void printGaji(){
        int gaji = 5000000;
        int THR;
       
        String tglMulaiKerja = "01-02-2021";
        String tipeGaji;
        String tglLebaran = "30-04-2021";

        int bulanMulaiKerja = getMonth(tglMulaiKerja);
        int bulanLebaran = getMonth(tglLebaran);
        int bulanBekerja =  bulanLebaran -bulanMulaiKerja;

        
       
        THR = (gaji * (bulanBekerja+1)) /12;

        LinkedHashMap<String, Integer> listGaji = new LinkedHashMap<String, Integer>();

        for(int i = bulanMulaiKerja ; i <= 12; i++){
            listGaji.put("1 " + getMonthName(i), gaji);
            
            if(i==bulanLebaran){
                listGaji.put("30 April 2021", THR);
            }
        }
        

        try{
            FileWriter karyawanFile = new FileWriter("Karyawan1.txt");
            for(String i:listGaji.keySet()){
                if(i == "30 April 2021"){
                    tipeGaji = "THR";
                } else{
                    tipeGaji = "Monthly";
                }
                karyawanFile.write(i + " - " + String.valueOf(listGaji.get(i) + " - " + tipeGaji + "\n"));
            }

            karyawanFile.close();

        }
        catch(IOException e){

        }
    }


    @Override
    public void run(){
        printGaji();
        
    }
    
}
